# Laboratory of network design and configuration

[[_TOC_]]

## Theory

### Fundamentals of IP Networking

- Internet

  ![](./res/img/1.png)

- Devices

  ![](./res/img/2.png)

- TCP/IP protocol stack

  ![](./res/img/3.png)

  - Example usage

    ![](./res/img/4.png)

- Router functions
  - Implements the first 3 layers (considering data link as two layers) of the TCP/IP protocol stack; main functions performed are the ones of Network layer
    - Main protocol of Network layer: Internet Protocol (IP)
      - The IP layer executes
        - Addressing
          - IP address
            - Identify routers and hosts: a host connected to more than a subnetwork (multi-homed, multi interface) has an IP address for each subnetwork/interface
            - Unique in the Internet (one exception, NAT)
            - 32 bits long
            - Hierarchical structure; a block of addresses is assigned to each sub network
              - IP Address = Net Id + Host Id
                - Host Id identifies the specific address of the block
                - Net Id identifies the sub network block (prefix)
                  - To identify the Net ID length the Subnet Mask Mask is introduced: 32 bit string having 1s for Net Id bits and 0s for Host Id bits
                  - Given the IP address of an host Add(H) and a subnet S represented by (Add(S), Mask(S)) to verify if H belongs to S: Add(H) & Mask(S) = Add(S)
              - Subnetting (classless): 

                ![](./res/img/5.png)

            - Network address (example): 192.168.1.0
            - Broadcast address (example): 192.168.1.255 
          - Classfull addressing (PAST)

            ![](./res/img/6.png)

        - Forwarding (and routing)
          - Host vs router

            ![](./res/img/7.png)

          - Routing table
            - Composed of (N M NH I) entries
              - N: IP address of the destination network
              - M: subnet mask of the destination network having N as address
              - NH: next hop router toward the destination
              - I: router host interface to which forward the packets directed to network N
            - Longest Prefix Matching rule
              - Logical and with the destination and netmask in order of entries in the list
              - Longest match is the path selected
            - Example

              ![](./res/img/8.png)

              - d.c. = directly connected
              - "Lines" are net interfaces
            - Default router/gateway: next hop for traffic directed to an unknown network

              ![](./res/img/9.png)

          - Host configuration
            - Parameters
              - Host name
              - IP address
              - Subnet mask
              - Default router
              - Server DNS
            - Configuration protocol: DHCP
            - Private IP addresses: NAT
        - Fragmentation
- Router
  - Cisco OS: IOS (Internetwork Operating System), uses a CLI
  - Hardware
    - CPU: executes IOS instructions
    - RAM: store routing table, cache fast switching, (running) configuration file, packet queues, IOS software executed
    - Flash: store the image of IOS
    - NVRAM: store the configuration to be used at startup (startup configuration file, running configuration files can become startup configuration files if saved)
    - Bus
      - System bus: providing communication among CPU and interfaces and used to forward packets among ingress and egress interfaces
      - CPU bus
    - ROM: stores in a permanent way the code used to perform hardware integrity checking (performed at startup)
    - Interfaces: connections among router and the "external world"
      - LAN
        - Connections with a switch or hub: straight-through cable (ethernet)
        - Connections with a router or a host: crossover cable (ethernet)
        - Allows the in-band router configuration: TELNET
      - WAN: can be of different types since different technologies (protocols) can be used
      - Console/AUX: management (uses baud rate)

    ![](./res/img/10.png)

### Packet Tracer and initial router configuration

- Packet tracer: emulate CISCO networking devices
  - Configure CISCO router
    - CLI
      - Connection
        - Console Session: physical
        - Telnet Session: from the same LAN (can also be from Rome to Bologna for example)
      - Hierarchical structure
        - User EXEC mode (view only mode)
          - `enable` highers the privilege
        - Privileged EXEC mode or enable mode (need password if coming from User)
          - `disable` lowers privilege
          - `exit` exits (`Ctrl-Z`)
          - `configure terminal` goes to global mode
        - Global Configuration mode

          ![](./res/img/11.png)

      - `?`: help
      - `no ip domain-lookup` to avoid CLI stops when a wrong command is typed (set it in `config` mode)
      - From Global configuration mode: `Router(config)#hostname Tokyo` -> changes hostname
      - Securing router access
        - Password can be defined for different access ways
          - Console port
            ```
            Router(config)#line console 0
            Router(config-line)#password <password>
            Router(config-line)#login
            ``` 
          - Telnet access, referred to as Virtual Terminal Line (vty) in the CLI
            ```
            Router(config)#line vty 0 4 // BETTER USING 0 15
            Router(config-line)#password <password>
            Router(config-line)#login
            ```
            - `0 4`: there are 5 telnet simultaneous possible connections, here i'm setting the password for all of them
          - Privileged EXEC mode
            - Methods
              - `enable password` (`Router(config)#enable password <password>`)
                - The password is shown (not encrypted) in the configuration files (visible using `show running-config` or `show startup-config`)
                - It is possible to encrypt the password (all the passwords) using the command `Router(config)#service password-encryption`
              - `enable secret` (`Router(config)#enable secret <password>`): algorithm more robust than the one used by the service password-encryption command
      - Show command: provide information about router features
        - `show interfaces`: statistics about interfaces
        - `show controllers serial`: hardware level information about interfaces
        - `show clock`: router clock
        - `show hosts`: list of devices (hostname and IP address) known by the router
        - `show users`: users connected to the router
        - `show history`: list of commands used in the past
        - `show flash`: information about flash memory and available IOS image files
        - `show version`: hardware level features of the router and running IOS
        - `show ARP`: ARP table of the router
        - `show protocol`: Layer 3 protocols configured at router level and at interface level
        - `show startup-configuration`: startup configuration file (saved into the NVRAM)
        - `show running-configuration`: running configuration file (saved into the RAM)
      - Negate a just inputted command: `no command name`
      - Save running config: `copy running-config startup-config`
      - Reload the startup configuration file from NVRAM: `Router# copy startup-configrunning-config`
      - Serial interface configuration (WAN)
        ```
        Router(config)#interface serial 0/0  // Define what interface to setup
        
        Router(config-if)#ip address <ipaddress> <netmask> 
        Router(config-if)#clock rate 56000  // Interfaces must be synchronized; (must be set just in at least one of the endpoint)
        Router(config-if)#no shutdown  // Interface up, default down
        ```
      - Ethernet interface configuration
        ```
        Router(config)#interface e0  // Define what interface to setup
        
        Router(config-if)#ip address <ipaddress> <netmask> 
        // No need for clock
        Router(config-if)#no shutdown  // Interface up, default down
        Router(config-if)#description <description>  // Interface description (optional)
        ```
      - Routing
        - Routing table of a router can be updated in two different ways
          - Using information exchanged with different routers (Dynamic routing - Routing protocols)
          - Using configuration commands executed by the network administrator (Static Routing)
            - Commands
              - `R1(config)#ip route Dest_IP_Add_Netw SubNet_Mask A/B`
                - `A`: the output interface (it is possible only in the case of a point-to-point link)

                  ![](./res/img/14.png)

                - `B`: next-hop router IP address (USE THIS USUALLY, LIKE ALWAYS)
              - Default route can be configured statically: `ip route 0.0.0.0 0.0.0.0 A/B`
            - Administrative distance (`AD`): measure of the reliability degree of a route
              - Low value of `AD` -> high reliability
              - Static routes are reliable by default (`AD = 1`)
              - Increase the administrative distance: `ip route Dest_IP_Add_Netw SubNet_Mask A/B AD`
        - Routing table
          - Show: `show ip route`
          - The rows (route) of a routing table have different sources
            - Directly connected routes (`C`)
            - Statics routes (`S,S*`)
            - Dynamic Routes (`RIP -> R, OSPF -> O`, etc...)
        - To check reachability: `ping` and `traceroute`
        - Netmask/IPs

          ![](./res/img/12.png)

          ![](./res/img/13.png)

### Routing protocols

- A routing protocol provides a communication channel among routers to exchange reachability information about networks
- Protocols
  - Routing Information Protocol (RIP)
  - Enhanced Interior Gateway Routing Protocol (EIGRP)
  - Open Shortest Path First (OSPF)
  - Border Gateway Protocol (BGP)
- Convergence
  - The performance of a routing protocol is the convergence speed
  - A routing protocol reaches the convergence state when all the network routers have the same network view
- Autonomous system (ex: Telecom Italia)
  - Set of networks and routers managed by the same network administrator
  - Identified by a single number (16 bit AS number)
  - Internet is composed of a set of ASes
  - Independent routing protocol for the routing inside each AS
  - Routing among ASes must be the result of an agreement among ASes
- Routing protocol classification
  - On the basis of functioning principles
    - Distance vector (simpler, slower): RIP, BGP
      - General
        - Info
          - Each router exchanges routing information only with neighbor routers (directly connected by a network)
          - Each router sends to the neighbors routers messages reporting its own routing table
          - Routing messages are sent periodically (the time period is a protocol parameter, 30 seconds for RIP)
          - Each row of the routing table sent has its own distance (depends on the protocol metric: in the case of RIP the metric is the number of hops)
          - A router computes its own routing table:
            - For each route two information are stored: distance and next-hop router
            - The directly connected routes have a distance equal to 0
          - When a routing table is received an algorithm is applied (Bellmann-Ford one for RIP) to update the routing table if new better routes (on the basis of the distance) are available.
        - Example
          1. Assign IP address and start configuring routing table

             ![](./res/img/15.png)

          2. Execute protocol: send routing table

             ![](./res/img/16.png)

          3. Update routing table (we are still missing something!)

             ![](./res/img/17.png)

          4. Next round of routing table exchange

             ![](./res/img/18.png)

          5. Update routing table

             ![](./res/img/19.png)

      - Routing Information Protocol (RIP)
        - Info
          - The number of hops (number of routers crossed) is used as distance to detect the best path
          - A distance equal to 16 means destination unreachable
          - Routing updates are sent in broadcast every 30 seconds
          - RIP messages are encapsulated into UDP packets, with source and destination ports both equal to 520
          - The administrative distance is 120
          - Can't be used in big networks because it will not converge fast, so the network will behave bad for a large time
          - Versions
            - Classful Routing Protocol (v1)
              - In　messages the subnet mask is not provided
              - How to choose subnet mask?
                - Using the default one on the basis of the classful approach (classes A, B or C)
                - The subnet mask of the network connected to the RIP message incoming interface is used
                - Major Network concept: class A, B o C address associated to an IP address
                    
                    ![](./res/img/21.png)
                  
                  - Example: major network of `192.168.0.128/25` is `192.168.0.0/24`
                  - How it works
                    - If a router receives an Update (RIP message) containing a destination network with IP address `A.B.C.D`, it associates the subnet mask following two rules
                      1. If the destination network has a major network different than the one of the network directly connected to the received interface, then the classful subnet mask is used
                      2. If the destination network has a major network equal to the one of the network directly connected to the received interface, then the same subnet mask of the interface is used

                      ![](./res/img/22.png)

                    - If a router has to send a RIP message through an outgoing interfaceand the destination IP address of the route is obtained by subnetting
                      1. If the destination address has a major network different than the one of the network directly connected to the interface, then the IP address of the major network is used (summarization)
                      2. If the destination network has a major network equal to the one of the network directly connected to the interface, then the same subnet mask of the interface is used

                      ![](./res/img/23.png)

                    - Problem
                      - Subnetting with variable mask length is not possible: SO RIP V1 DOESN'T WORK HERE
                      - Networks with same major network must be contiguous: SO RIP V1 DOESN'T WORK HERE

                      ![](./res/img/24.png)

              - Classful but it works even if subnetting is used IF subnetting is performed with fixed subnet length AND the subnetted networks are contiguous
              - Configuration
                - `router rip`: enable RIP
                - `network NetAddrOfInterface`: define the interfaces running RIP
                  - Interfaces running RIP will send and receive RIP messages
                  - Networks (directly) connected to interfaces running RIP will be inserted (as routes) in the RIP messages sent
                  - Usually enabled in all interfaces
                - RIP route in the routing table

                  ![](./res/img/20.png)

                - RIP timers
                  - Invalid timer: if a route is not updated after `invalid timer` seconds (default value 180s), it is flagged as invalid and its distance is set to 16
                  - Flush timer: if a router is not updated after `flush timer` seconds (default value 240s), it is removed from the routing table
                - `show ip protocols`: show info about routing protocols
                - `passive-interface`: avoid sending RIP messages through a specific interface; the router still inserts the network connected to the passive interface in the RIP messages
                - `debug ip rip`: print on the terminal what is happening in RIP protocol
            - Classless Routing Protocol (v2)
              - In messages subnet mask field is present
              - Configuration commands
                ```
                R3(config)# router rip
                R3(config-router)# version 2
                # In the case of variable or discontinuous subnetting, RIPv2 doesn’t work
                # We must disable auto-summary (makes v2 behave live v1)
                R3(config-router)# no auto-summary
                ```
    - Linkstate (complex, faster): OSPF
  - On the basis of use scenario:
    - InteriorGateway Protocol (IGP): intra-AS (RIP and OSPF)
    - ExteriorGateway Protocol (EGP): inter-AS (BGP)
- For all routing protocols this applies: default router distribution
  - It is possible to configure the default router (to reach internet) only on the border router and then to distribute it to remaining routers using RIP: `Router (config-router)# default-information originate` on the border one

    ![](./res/img/25.png)

  - It is also possible to distribute all static routes (except the default one): `Router (config-router)# redistribute static`

### Open Shortest Path First (OSPF)

- General
  - Intra-AS routing protocol (IGP)
  - Link-state routing protocol: each network router knows the network topology
  - OSPF packets are directly encapsulated into IP packets, with protocol field equal to `89`
  - Administrative distance: `110`
- Principles
  - Each router periodically send (default: 10s) Hello packets, to notify its links status to the neighbors
  - Each router describes its topology generating a Link State Advertisement (LSA) packet
  - The LSA is sent to all network routers using the flooding technique
  - Each router has a global network view (LSA database) and it is able to compute the set of shortest paths (Shortest Path Tree: SPT) executing the Dijkstra algorithm
- Elements of an AS
  - Routers
  - Networks: classified in Transit Networks (more than one router) and Stub Networks (connected to a single router)
  - Example

    ![](./res/img/26.png)

    - Link going from a network to a router has cost equal to `0`
- Link State Advertisement (LSA)

  ![](./res/img/27.png)

    - Link State Header
      - Type: identifies the different LSAs
        - Router_Lsa (type 1)
        - Network_Lsa (type 2)
        - ...
    - Link State Data
- Flooding
  - LSA received on an incoming interface is forward to all the remaining interfaces
  - Each LSA has a sequence number
    - Avoid the forwarding of "old" LSAs (already forwarded)
    - Update the LSA database only if needed
- Shortest Path Tree computation
  - Router maintains a database with the latest data on network topology
  - Network topology is a weighted graph
    - Nodes are routers and networks
    - Links are
      - Point-to-point links among routers
      - Links among a router and a network
  - Each router computes the Shortest Path Tree (Dijkstra) and it is able to update its routing table
- Example

  ![](./res/img/28.png)

  ![](./res/img/29.png)

- Configuration
  - Access configuration: `R(config)# router ospf id` (`R(config)# router ospf 1`)
    - `id`: `[1 –65535]`, local validity
      - Identifier of OSPF process; we will always use the same one (`1`)
  - Interfaces (and so networks) on which enable OSPF: `R(config-router)# network network-address wildcard-mask area area-number` (`R(config-router)# network network-address wildcard-mask area 0`)
    - `wildcard-mask`: obtained from netmask inverting 0s and 1s (`255.0.0.0` -> `0.0.0.255`)
    - `area-number`: an AS can be divided into areas to solve scalability issues; area `0` is always present (we will just use it)
    - `passive-interface interface` can be used also here
  - Link cost (set in INTERFACE configuration)
    - `R(config)# interface FastEthernet X/Y`
    - `R(config-if)# ip ospf cost cost`
    - Default costs per interface

      ![](./res/img/30.png)

    - Remember that to avoid using a link the cost of that link should be higher than the highest cost of alternative paths    
  - Verifying
    - `R# show ip ospf neighbor`
    - `R# show ip protocols`
    - `R# show ip ospf database`
- Multi-area OSPF
  - Scalability issues solved using areas
    - AS is divided into areas
    - In each area, the "internal" routers exchange LSAs
      - Different Databases in different areas
      - Flooding is restricted to the area
      - Edge routers exchange their databases (inter-area router)
  - You always need an "area 0" which is connected with all other areas
  - Configuration
    - ABR: association between interfaces and areas
      - `R(config)# interface X Y/Z`
      - `R(config-if)# ip ospf OSPF_ID area area ID`
    - The network of different areas will be identified in the routing table with the following acronym: `O IA`

  ![](./res/img/31.png)

### Dynamic Host Configuration Protocol (DHCP)

- DHCP protocol
  - Allows to dynamically configure hosts in a LAN
  - Configuration
    - Parameters
      - IP address and subnet mask (IP address range)
      - Default gateway
      - DNS Server
    - Commands
      - Start DHCP configuration: `Router(config)# ip dhcp pool NAME_POOL`
        - `NAME_POOL`: name used to identify the DHCP configuration on the router
      - Definition of the address pool to be assigned dynamically: `Router(config-dhcp)# network NET_ADDRESS NETMASK`
        - Ex: `192.168.1.0/24` -> `Router(config-dhcp)# network 192.168.1.0 255.255.255.0`
      - Exclude IP address from the pool: `Router(config)# ip dhcp excluded-address IPADDRESS(ES)`
        - If the initial 49 addresses should be excluded: `Router(config)# ip dhcp excluded-address 192.168.1.1 192.168.1.49` 
      - Default gateway: `Router(config-dhcp)# default-router 192.168.1.1`
      - DNS: `Router(config-dhcp)# dns-server 192.168.1.10`
    - Client: tick DHCP config in Desktop -> IP configuration

### Network Address Translation (NAT)

- IP addresses are not enough to assign a unique IP address to each network host/device; NAT is the solution (private IP addresses)

  ![](./res/img/32.png)

  - Modify (translate) the IP addresses (and other header fields) of outgoing and incoming packets
  - Private addresses table

    ![](./res/img/33.png)

- Types
  - Static: one-to-one translation

    ![](./res/img/34.png)

  - Dynamic: N-to-one translation

    ![](./res/img/35.png)

    - Breaks the network stack
- Configuration
  - FIRST: Setting of inside (local) and outside (global) interfaces
    - `Router(config-if)# ip nat inside`
    - `Router(config-if)# ip nat outside`
  - Static NAT: `Router(config)# ip nat inside source static <private_address> <public_address>`
  - Dynamic NAT
    - Number of available public IP addresses is lower than the number of hosts (private IP addresses)
      - Definition
        - Private IPs: `Router(config)# access-list <access-list-number> permit <source_address> <wildcard>`
          - `source_address`: network IP address (private)
          - `wildcard`: netmask wildcard (replace 0 with 1 and vice versa)
        - Public IPs (pool of public IPs): `Router(config)# ip nat pool <name> <start-ip> <end-ip> netmask <netmask>`
      - Translation rule: `Router(config)# ip nat inside source list <acl-number> pool <name>`
        - The router uses ports-ips couples dynamically
        - Let's suppose we have like a million private hosts: one single IP address is not sufficient (port numbers are limited)  
    - Use a single public IP address for all the LAN hosts: the public IP address of the router
      - In this case the pool of public IP addresses is not required
      - Translation rule: `Router(config)# ip nat inside source list <acl-number> interface <interface> overload`
  - Show ip nat translations: `show ip nat translations`

### Virtual LAN (VLAN)

- Ethernet (IEEE 802.3): standard for LANs
  - CSMA/CD (Carrier Sense Multiple Access/Collision Detection)
    - Carrier Sense: all devices have to sense the medium before sending frames
    - Multiple Access: collisions
    - Collision Detection: Back-off algorithm (retransmit algorithm)
  - Frame

    ![](./res/img/36.png)

    - MAC addresses are used
  - Devices
    - Hub: forwards frames on all interfaces
    - Switch: forwards frames only on the proper outgoing interface (the one to reach the destination); uses a MAC forwarding table
      - MAC forwarding table is populated by means of a MAC learning procedure

        ![](./res/img/37.png)

        - "The MAC for PC1 is reachable using Port 1"
      - Basic configuration
        - We can assign to a switch 
          - IP address: used for switch management (telnet connection)
          - Subnet mask: used for switch management (telnet connection)
          - Default gateway: provide access to an external web/tftp server (mainly to download a new IOS version)
        - These parameters will be assigned to the whole switch, NOT TO AN INTERFACE
        - How to

          ![](./res/img/38.png)

          1. Create virtual interface
          2. Couple real and virtual interface
- VLAN
  - Definition
    - Sharing of the same physical infrastructure among different IP networks
    - IP network sharing a physical network with different VLANs
  - Advantages and example

    ![](./res/img/39.png)

    - PC1 can talk only with PC4
    - PC2 can talk only with PC4
    - PC3 can talk only with PC6
  - Configuration
    - Assign id and name: `Switch (config)# vlan NUM` -> `Switch (config-vlan)# name NAME` (optional)
      - Each VLAN is identified by a number (`VLAN ID`: `1 - 1005`) and has its own address block
      - The VLANs of a network must be defined in all the switches
    - Switches ports

      ![](./res/img/40.png)

      - Port-based VLAN: access port are statically associated to a specific VLAN
        - `Switch(config)# interface FastEthernet1/0`: set interface
        - `Switch(config-if)# switchport mode access`: couple interface and access port
        - `Switch(config-if)# switchport access vlan X`: couple access port and vlan
      - How to associate a VLAN to a received frame on a trunk port?
        - 802.3Q -> tag is inserted in the ethernet header to insert the VLAN ID
        - Trunk ports configuration
          - `Switch (config)# interface FastEthernet o/x`: set interface
          - `Switch(config-if)# switchport mode trunk`: set interface as trunk
          - `Switch(config-if)# switchport trunk native vlan 99`: set native virtual lan (OPTIONAL) -> just set that a vlan is the "canonical", it doesn't need to be tagged, it is the default one
      - Filter VLANs from trunk
        - `Switch(config-if)# switchport trunk allowed NUM-NUM`
  - Management
    - `show vlan brief`: list all the VLANs configured and the associated access interfaces
    - `show interface FastEthernet x/y switchport`: check the mode of an interface (name, number, is native, trunk or access port...)
  - Troubleshooting
    - Native VLAN mismatch
    - Trunk mode mismatch
    - Wrong IP addresses for VLAN hosts
    - VLAN not allowed on trunk ports
  - Inter-VLAN routing: router must be present
    - Two different ways to implement inter-VLAN routing
      - Traditional Inter-VLAN: we must use a lot of interfaces!
        - How to
          - A router must be connected to a switch through a certain amount of physical interfaces; the number of router-to-switch physical links is equal to the number of VLANs able to communicate to each other
          - Each router interface is associated to a VLAN so an IP address of the VLAN block must be assigned to it
          - The switch ports connected to the router must be configured in access mode
        - Example: only VLAN 10 and VLAN 30 are allowed to communicate each other

          ![](./res/img/41.png)

          - Router
            - `R1(config)# interface Fa0/0`
            - `R1(config-if)# ip address 172.17.10.1 255.255.255.0`
            - `R1(config-if)# no shutdown`
            - `R1(config)# interface Fa0/1`
            - `R1(config-if)# ip address 172.17.30.1 255.255.255.0`
            - `R1(config-if)# no shutdown`
          - Switch
            - `S1(config)# interface Fa0/6`
            - `S1(config-if)# switchport mode access`
            - `S1(config-if)# switchport access vlan 10`
            - `S1(config)# interface Fa0/5`
            - `S1(config-if)# switchport mode access`
            - `S1(config-if)# switchport access vlan 30`
          - PCs: configure also the default gateway
      - "Router-on-a-stick" Inter-VLAN: single interface used
        - How to
          - The physical interface of the router is split into virtual interfaces (subinterfaces): the number of virtual interfaces is equal to the number of VLANs able to communicate each other
          - Each subinterface is associated to a single VLAN
          - The switch port connected to the router must be configured in trunk mode
        - Example: only VLAN 10 and VLAN 30 are allowed to communicate

          ![](./res/img/42.png)

          - Router: interface connected to the switch must be split in two subinterfaces
            - `R1(config)# interface Fa0/1.10`
            - `R1(config-subif)# encapsulation dot1q 10`
            - `R1(config-subif)# ip address 172.17.10.1 255.255.255.0`
            - `R1(config)# interface Fa 0/1.30`
            - `R1(config-subif)# encapsulation dot1q 30`
            - `R1(config-subif)# ipaddress 172.17.30.1 255.255.255.0`
            - `R1(config)# interface Fa0/1`
            - `R1(config-if)# no shutdown`
          - Switch: port connected to the router is configured in trunk mode
            - `S1(config)# interface Fa0/5`
            - `S1(config-if)# switchport mode trunk`
          - PCs: configure also the default gateway

### Access Control List (ACL)

- Filter the traffic using a rule (a sort of firewall)
  - The ACL can be applied for incoming packets (inbound) or outgoing packets(outbound)
  - Rules to determine if packets must be processed and forwarded or blocked and dropped by the router
  - Configured on the router and applied to interfaces to control network access
    - Can be associated also to a sub-interface
  - Defined on the basis of the IP addresses, protocol, direction, port, etc...
- How it works
  - An ACL is an ordered list of rules

    ![](./res/img/43.png)

  - Remember that if we start configuring it the default is DENY; if ACLs aren't configured then default is PERMIT
  - ACLs must be as simple as possible
- Creation
  - Types
    - Standard: packets filtering is based only on the source IP address (identified by a unique router-level number, numbered ACL, different range from extended)
    - Extended: packets filtering is based on source and destination IP address, protocol, and destination port (identified by a unique router-level number, numbered ACL, different range from standard)
    - Named: can be both Standard and Extended, are identified by a name and provide a more flexible configuration procedure
  - Commands
    - General
      - General
        - `access-list NUM`
          - After the definition, an ACL must be associated to one (or more) interface(s) on a specific direction
        - `no access-list NUM`: remove ACL `NUM`
          - WE CAN'T REMOVE JUST ONE CONDITION
        - `ip access-group NUM|NAME IN-or-OUT`: associate an ACL to an interface (in the interface configuration mode)

        ![](./res/img/44.png)

      - Wildcard mask: define the matching of an ACL rule regarding IP addresses
        - Example
          - `172.16.0.0   0.0.255.255`:
            - `172.16.2.3`: matching
            - `172.15.0.1`: no matching
        - Ant/host
          - `any`: `255.255.255.255` for the wildcard mask
            - Always a matching, independently of the IP address
          - `host`: `0.0.0.0` for the wildcard mask
            - All the bits of the IP address must be checked
      - Verify ACL
        - `show ip interface`: see ACLs on interfaces
        - `show access-list`: show all ACLs
        - `show running-config`
    - Standard: filters on the basis of the source IP address (range `[1, 99]`), remember that order matters and is not possible to switch rules after having added them
      - Add: `Router(config)#access-list NUM {deny | permit} SRC [wildcard]`
      - Remove: `Router(config)#no access-list NUM`
    - Extended: filter traffic considering IP source and destination addresses, the protocol and the destination port number (range `[101, 199]`)
      - Add: `Router(config)#access-list NUM {deny | permit} PROTO SRC [SRC-MASK DST DST-MASK OPERATOR OPERAND] [established]`
        - Operators
          - `PROTO`: IP, TCP, UDP, ICMP, GRE, IGRP
          - `SRC` and `DST`: source and destination address
          - `SRC-MASK` and `DST-MASK`: source and destination wildcard of netmasks
          - `OPERATOR OPERAND` lt, gt, eq, neq and a port number
          - `established`: allow if connection established
        - Port numbers help

          ![](./res/img/46.png)

        - Examples

          ![](./res/img/45.png)

      - Remove: `Router(config)#no access-list NUM`
    - Named: identified by a name (can be standard or extended)
      - **Can be modified with no need of starting from scratch**
      - Add: `ip access-list {extended | standard} NAME`
        - `{permit | deny} PROTO SRC SRC-WILDCARD [OPERATOR [PORT]] DST DST-WILDCARD [OPERATOR [PORT]] [established] [precedence PRECEDENCE] [tos TOS] [log] [time-range TIME-RANGE-NAME] `
      - Remove: `Router(config)#no access-list NAME`
      - Edit

        ![](./res/img/47.png)

  - General rules
    - The standard ACLs should be put as close as possible to the destination (since it is not possible to specify the destination)
    - The extended ACLs should be put as close as possible to the source (to filter earlier the traffic, and so reducing routers work)
  - Limit telnet access

    ![](./res/img/48.png)

    - Create: set who has telnet access
    - Apply: apply the rule

### LAN security

- LAN attacks
  - Attacks
    - MAC flooding
      - If an incoming frame with a new MAC source address is received, the switch add a row in the forwarding table
      - If an incoming frame has a destination MAC address not present in the forwarding table, the switch acts as an hub
      - The forwarding tables have a limited size
      - Attack: frames with artificial source MAC address -> forwarding table is saturated -> frames with new MAC destination address are forwarded in broadcast
    - DHCP spoofing: a malicious DHCP server is inserted in the LAN, so that fake info (default gateway) are notified to LAN hosts (uses MITM)
    - DHCP starvation: attack to the DHCP servers, sending a huge amount of DHCP requests so that to use all the available IP addresses 
  - Contermeasures
    - Port Security: configured on switch interface/s
      - Idea
        - Limit the end devices that can be connected to a specific switch interface
        - The security policy is based on the source MAC address of incoming packets and on the number of different source MAC addresses allowed on the interface
      - How it works
        - If a frame having a MAC source address not allowed is received, the interface switch to Violation Mode:
          - Shutdown by default (error disabled state)
          - To recover from the error state it is necessary to manually shutdown the interface -> `shutdown` and then `no shutdown`
            - Can auto-recover: `S1(config-if)# err disable recovery interval TIME`
              - `TIME`: `[30-86400]`
        - It is possible to allow the access to a single MAC address or to a range of MAC addresses
        - The association among the interface and the allowed MAC address/es can be **dynamic** or **static**
      - Configuration
        - `Switch(config-if)# switchport port-security`
        - MAC address of a PC: `ipconfig /all`
        - Configuration modes
          - Static: `Switch(config-if)# switchport port-security mac-address mac-address`
          - Dynamic: MAC addresses are learned dynamically up to a fixed number (1 by default) and saved only in the secure MAC address table
            - `S1(config-if)# switchport mode access`
            - `S1(config-if)# switchport port-security`
            - `S1(config-if)# switchport port-security maximum 50`
          - Sticky Dynamic: the allowed MAC addresses are learned dynamically up to a fixed number and saved in the secure MAC address table and in the running configuration file
            - To dynamic commands add at the end: `S1(config-if)# switchport port-security mac-address sticky`
        - Show config (in the switch main page): `show port-security interface INTERFACE` -> tells also if it is in violation mode
        - Show MAC table (in the switch main page): `show port-security address`
- VLAN attacks
  - Attacks
    - Gain VLAN access by configuring a host to spoof a switch
      - Creating a trunk with an unsecured switch
      - Exploiting the auto trunking (DTP) configured by default on switch ports
  - Countermeasures
    - Explicitly configure access ports
    - Disable auto negotiate on trunks (`switchport nonegotiate`)
    - Manually enable trunk links (`switchport mode trunk`)
    - Disable unused ports, make them access ports, and assign to a black hole VLAN
    - Change the default native VLAN
    - Implement port security
- DHCP attacks
  - Attacks
    - DHCP spoofing: attacker configures a fake DHCP server on the network to issue IP addresses to clients
    - DHCP starvation: attacker floods the DHCP server with bogus DHCP requests and leases all of the available IP addresses (DoS, new clients cannot obtain an IP address)
  - Countermeasures
    - Configure DHCP snooping
      - Switch will deny packets containing unauthorized DHCP server messages coming from an untrusted port
      - Snooping recognizes two types of ports:
        - Trusted ports: ports connecting to upstream DHCP servers
        - Untrusted ports: connect to hosts that should not be providing DHCP server messages
      - How to
        1. Enable DHCP snooping globally: `S1(config)# ip dhcp snooping`
        2. Enable DHCP snooping on selected VLANs (at least one): `S1(config)# ip dhcp snooping vlan VLANID`
        3. Configure trusted interfaces (untrusted is default): `S1(config-if)# ip dhcp snooping trust`
        4. Disable option 82 insertion by switch: `S1(config)# no ip dhcp snooping information option` 
    - Configure port security
- VPNs
  - Idea
    - Private network created over a public network (Internet) exploiting the tunneling mechanism
    - Tunneling: encapsulation of a packet (frame) with an outer header
    - Can also provide security functions, such as encryption: IPsec
    - VPN gateway is necessary: it could be a router or a dedicated device
      - Responsible for encapsulating and encrypting outbound traffic (and decapsulatingand decrypting inbound traffic)
  - GenericRouting Encapsulation (GRE)
    - What is
      - Non-secure, site-to-site VPN tunneling protocol
      - In the outer IP header, 47 is used in the protocol field
      - Encapsulation uses a protocol type field in the GRE header to support the encapsulation of any OSI Layer 3 protocol

      ![](./res/img/49.png)

    - Configuration
      - Create a tunnel interface (not all router’s models support it!): `R(config)# interface Tunnel NUMBER`
      - Configure GRE as the tunnel interface mode: `R(config-if)# tunnel mode gre ip`
      - Assign an IP address to the tunnel interface (usually a private address): `ip address 192.168.2.1 255.255.255.252` (example)
      - Specify the tunnel source physical interface: `R(config-if)# tunnel source INTERFACE`
      - Specify the tunnel destination IP address (IP address of the physical interface of the tunnel end point): `R(config-if)# tunnel destination IPADDR`
      - Check tunnels
        - `show ip interface brief | include Tunnel`
        - `show interface Tunnel NUMBER`



## Exercises

- [Exercises](./res/ex/)