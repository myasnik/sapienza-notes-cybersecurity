# Crypto

[[_TOC_]]

## Theory

### Other summaries sources

- https://github.com/asmeikal/crypto16/blob/master/notes/root.pdf

### Information-Theoretic Cryptography

#### Perfect secrecy, one-time pad, Shannon's theorem

##### Message confidentiality and integrity

- Properties of a message to be secure

  ![](./res/img/1.png)

  - Here the channel (and in general always) is considered insecure (Eve can read what is traversing the channel)
- Approaches to solve these two points
  - Asymmetric crypto
  - Symmetric crypto

##### Symmetric crypto and perfect secrecy

- Algorithms should be public!

  ![](./res/img/2.png)

  - `pi` is the general algorithm that contains encryption (`Enc`) and decryption (`Dec`) algorithms
  - `m` is the message
  - `M` is the space of messages (titpically binary strings of a arbitrary length), messages distribution (the random variable)
  - `c` is the ciphertext
  - `C` is the space of ciphertexts (titpically binary strings of a arbitrary length), ciphertext distribution (the random variable)
- Secret key crypto

  ![](./res/img/3.png)

  ![](./res/img/4.png)

  - Why the `$` means random? Because sometimes in algorithms the randomness is called "coins"
  - `lambda` is fixed usually
  - Every key is chosen with the same probability (uniform distribution)
  - **Perfect secrecy by Shannon**: how `pi` can be private, we want that ciphertext reveals NOTHING about plaintext
    - Definition 

      ![](./res/img/5.png)

      - Notes
        - `M` is the distribution of the message, for example if i speak english the message will be in english so its distribution will be different from a message in italian
        - `calligraphic M` is all possible messages (the messages space)
      - Even knowing the ciphertext the probability of choice of the message is still the same, in other words ciphertext reveals nothing about plaintext
      - This is impractical 
    - Theorem: following definitions of perfect secrecy are equivalent

      ![](./res/img/7.png)

    - Proofs
      
      ![](./res/img/8.png)

      - So `m` and `c` are independent (product)

      ![](./res/img/9.png)

      ![](./res/img/10.png)

      ![](./res/img/11.png)

      ![](./res/img/12.png)

      ![](./res/img/13.png)

      ![](./res/img/14.png)

      - We can do the last step because we proved that the two ones simplified are the same

##### Perfect secrecy implementation: One time pad

- Application: One-time pad (uses xor)

  ![](./res/img/6.png)

  - Theorem: one-time pad is perfectly secret
  - Proof (using definition 3)

    ![](./res/img/15.png)

  - Limitations

    ![](./res/img/16.png)

    - Shannon said that its proof works just for ONE ciphertext, if i have more then it is a problem! (I could infer something, that's why it is "one time")
    - So we have to change key for every message we encrypt
    - These holds for all perfectly secret algorithms!

##### Shannon's Theorem 

- Theorem

  ![](./res/img/17.png)

  - Length of the key must be at least the length of the message
- Proof (proof by contraddiction)

  ![](./res/img/18.png)

  ![](./res/img/19.png)

  - This because `m` is outside `M'` and this contains all possible decryptions of `C`

#### Perfect authentication, universal hashing, extractors, leftover-hash lemma

##### Message authentication in perfect secrecy (perfect authentication)

![](./res/img/20.png)

- `m` is public but the adversary can't manipulate it (without people knowing it)
- The strange symbol is the authenticator of the message (the "sign")

![](./res/img/21.png)

![](./res/img/22.png)

##### Information-theoretic MAC

- Definition

  ![](./res/img/23.png)

  ![](./res/img/24.png)

##### Pair-wise independent hashing

- Definition: construction for pairwise independent hashing

  ![](./res/img/25.png)

  - `H` is a family of functions (a function for each `k`)
  - For 3 messages the joint distribution is not uniform
  - Theorem

    ![](./res/img/26.png)

  - Proof

    ![](./res/img/27.png)

    ![](./res/img/28.png)

  - Construction

    ![](./res/img/29.png)

    ![](./res/img/30.png)

    ![](./res/img/31.png)

    - `Zp` is a set of integers from `0` to `p - 1`; if you get a number `> p` then you do `mod p`
    - `1/p^2` because the right part of the last probability is a costant so the probability that `a b` is equal to a constant is `1/p^2`
    - This satisfies the definition of pairwise independence

##### Randomness extraction and the Von Neumann extractor

- Randomness crucial in crypto
  - Generate uniform keys
  - Most crypto algorithms are randomized (non deterministic): if you run this algo on the same input twice you get different outputs
- How do we generate good randomness?
  - From physical processes (not truly random): people writing on a keyboard or moving a mouse
- How to turn a "somewhat random" source intro truly random sources (truly random means uniformly distributed)1?
  - Definition: von Neumann extractor

    ![](./res/img/34.png)

    ![](./res/img/35.png)

    ![](./res/img/36.png)

    ![](./res/img/37.png)

    - Biased = not uniform
    - `2p(1-p)` because is either `b1 = 1 and b2 = 0 or b1 = 0 and b2 = 1` (we discard cases when `b1` and `b2` are the same) 
    - But we imposed a source of randomness (biased distribution) that is too good, let's try with a weaker one (see next, real life)

##### Min entropy

- Definition
  - Physical processes produce X which is unpredictable

    ![](./res/img/38.png)

    - Intuition rephrased: measures the amount of unpredictability in X (how much X is random)
  - Examples

    ![](./res/img/39.png)

    - Constant means that outputs always the same thing

    ![](./res/img/40.png)

  - Real life: this request is IMPOSSIBLE :(

    ![](./res/img/41.png)

    ![](./res/img/42.png)

    - Maximizes in the sense that we have to find the preimage that has the higher number of elements
      - Either `0` has more preimages than `1` or the opposite (or they have the same)

    ![](./res/img/43.png)

      - Take the set with max cardinality and define `X` uniform on this set

    ![](./res/img/44.png)

    - Simple explanation

      ![](./res/img/427.jpg)

##### Seeded extractor

- Definition: seed can be public, method to achieve the last explained thing

  ![](./res/img/45.png)
  
  - **Take a family of extractors and not just one**
  - S = seed, it is uniform (truly random): we just have it
  - SD (statistical distance): distance between two distributions
  - More or less uniform: an attacker can't distinghuish the seeded extractor distribution and an uniform distribution (statistical distance is small)

##### Leftover hash lemma

- Theorem

  ![](./res/img/46.png)

- Lemma

  ![](./res/img/47.png)

  - Col: collision probability
- Proof (theorem): not necessary to learn it, just to make you understand

  ![](./res/img/48.png)

  ![](./res/img/49.png)

  ![](./res/img/50.png)

  ![](./res/img/51.png)

- Proof (lemma): not necessary to learn it, just to make you understand

  ![](./res/img/52.png)

  ![](./res/img/53.png)

  ![](./res/img/54.png)

  - Product between vectors = inner product

  ![](./res/img/55.png)

  - Sum divided cardinality of y disappears

  ![](./res/img/56.png)

  ![](./res/img/57.png)

- From now on we will consider that randomness will come for free!

### Computational Security

#### One-Way Functions (OWF) and complexity theory

##### Asymptotic security (negligible and polynomial functions, PPT algorithms)

- Statistical security comes with some inherent limitations
  - "Tiny" probability of security breach: maybe the adversary can distinguish the distribution (see above) with some probability which is really low, so the crypto algorithm could also be not perfect but almost perfect
  - Adversary must be "efficient"
- Approaches
  - Concrete security: build cryptosystems such that no adversaries running in time t (t steps) can break security with probability better than epsilon (ex: `2^-80`)
  - Asymptotic security

    ![](./res/img/58.png)

    - Lambda is the security parameter: how much security you want
    - In other words polynomial is used to say that there exist a polynomial that is the upper bound of a defined function
    - For larger enough lambda then this function can be upperbounded by some polynomial in lambda

    ![](./res/img/59.png)

    - Negligible if always upperbounded by `1/p(lambda)` (every inverse polynomial)

    ![](./res/img/60.png)

    - What if instead A is wrong with probability `1/k = 1/lambda` (where `k` is input size) but you don't know if the output is correct? Only way is to take large `k = lambda`, `lambda = k = 2^80`
    - By above observation, our approach:
      - Efficient = polynomial
      - Error only with negligible probability

      ![](./res/img/61.png)

      ![](./res/img/62.png)

      ![](./res/img/63.png)

      - PPT (probabilistic polynomial time): a turing machine A is PPT if its worst case running time is polynomial (in input length) 
- From now on we will prove conditional statements and our definitions and problems will be based on known "hard problems"; so that if we base our assumptions to build a crypto system on hard problems then until these hard problems are unresolved our crypto system is secure
  - A problem is easy if you can solve it in polynomial time
  - A problem is hard if you can not solve it in polynomial time (except with negligible probability)
- The approach is if i want to prove that the scheme is secure i show that if it is not secure (adversary exists and is efficient) then i can solve the hard problem

##### One-way functions

- Definition

  ![](./res/img/64.png)

  ![](./res/img/65.png)

  - Game based on the length, (i think that) the longest is the `x` and `f(x)` the hardest is finding the preimage 

##### Impagliazzo's world

![](./res/img/66.png)

- Inverting factorization can't be solved in polynomial time YET (nobody has found a way YET); so probably there isn't a way (that's why we aren't sure that OWF exist)
- `P` = problems that can be solved in polynomial time
- `NP` = problems that can't be solved in polynomial time
- `P = NP` correlates to "do OWF exist?"; if not then `P = NP`, else `P != NP` (i think)

![](./res/img/67.png)

- Key exchange: share key over public channel
- Avg-hard puzzles: hard problems
  - Yes: they exist and a solution can be found
  - No: they exist and a solution can't be found

### Symmetric crypto

#### Pseudorandomness

- Notes of context
  - We would like to have symmetric crypto with a key that can be used a lot of times (an arbitrary amount of times) and that its length doesn't depend on the message length (compared to the message the key is smalls); the adversary is supposed to be a turing machine with polynomial running time
  - With this we will use one way functions
- What is pseudorandomness?
  - Something that is random from the point of view of an attacker with polynomial running time

##### Pseudorandom Generators (PRG) and a note about Computational indistinguishability

- Definition

  ![](./res/img/69.png)

  ![](./res/img/70.png)

  - `G` and `U` are respectively a function over a uniform distribution and a uniform distribution itself (so the result of function `G` is "almost" uniform)
  - What does the circled symbol means?
    - Imagine two games (black and red): the adversary can't distinguish red from black (and vice-versa); he can distinguish them with negligible probability

      ![](./res/img/71.png)

    - Property 3 "definitions": the adversary responds in some way to problems, its response distribution defines if he distinguished the two `z`
      - First

        ![](./res/img/72.png)

      - Second

        ![](./res/img/73.png)

      - Third

        ![](./res/img/74.png)

##### One-time computational security for Secret Key Encryption (SKE)

- Plan to further define

  ![](./res/img/77.png)

  - Start with 2
    - What is secure encryption (computationally secure)?
      - Ciphertext reveals nothing about the plaintext: in other words if Alice encrypts something that has a defined distribution end publishes the encrypted message and a distribution the attacker will not be able to define if the given distribution is the one of the plaintext

      ![](./res/img/78.png)

      ![](./res/img/79.png)

      ![](./res/img/80.png)

      - This is one-time; but if the adversary sees two ciphertexts? Not safe
    - Note: definitions must be constructed well!

      ![](./res/img/82.png)

      - In other words this just says that FOR EVERY A we are safe, it's just the proof of the fact that the above definition is correct
    - Example algorithm construction based on PRG

      ![](./res/img/84.png)

    - Theorem

      ![](./res/img/85.png)

    - Proof

      ![](./res/img/86.png)

      - The difference between the two games is that in the black one the challenger picks the seed, expands it and then encrypts; in the red one we do one-time pad
      - Lemma

        ![](./res/img/87.png)

        - Proof lemma 1

          ![](./res/img/90.png)

          ![](./res/img/91.png)

          - So A can not exist
          - We call B a reduction (we are reducing A to B): **this method will be largely used from now on!**
            - Definition

              ![](./res/img/92.png)

            - Analysis

              ![](./res/img/93.png)

              - This equation implies

                ![](./res/img/91.png)

        - Proof lemma 2

          ![](./res/img/94.png)

      ![](./res/img/88.png)

    - Technique: hybrid argument

      ![](./res/img/95.png)

      ![](./res/img/96.png)

##### Constructing PRGs, hard-core predicates, Goldreich-Levin theorem and One-Way Permutations

- Remember "Plan to further define -> Point 1"

  ![](./res/img/100.png)

  - We aren't gonna prove it because it is too hard, we are gonna prove OWPs (one way permutations)
  - Approach

    ![](./res/img/101.png)

    1. If i can stretch one bit i can stretch how many bits i like
       - Theorem
       
         ![](./res/img/102.png)

       - Proof

         ![](./res/img/103.png)

         - In other words if you do this stretch for one bit and then the result is given again to the `G` function that stretches one bit this means that it can be stretched arbitrarily (until it is polynomial)
         - But there is a problem: the first time we run `G` it's on a seed that is random, the next times the seed is pseudorandom; this should be fine because if we could distinguish it we could also distinguish the `G` based on the truly random seed (not possible)
           - Let's prove this (using hybrid argument) 

             ![](./res/img/104.png)

             ![](./res/img/107.png)

             - I'm showing that all the intermediate pieces "converge" to our thesis

             ![](./res/img/105.png)

           - Now i can do the proof by reduction

             ![](./res/img/106.png)

             ![](./res/img/108.png)

             ![](./res/img/109.png)

             - This proves lemma and theorem
    2. Step 2

       ![](./res/img/110.png)

       - Definition

         ![](./res/img/111.png)

         - `f`: one way function
         - `h`: predicate
         - `h(x)` is uniform even if you know `f(x)`

         ![](./res/img/112.png)

         ![](./res/img/431.jpg)
       
       - Theorem (Goldreich - Levin): in practice nobody uses this

         ![](./res/img/113.png)

         - Corollary and proof

           ![](./res/img/114.png)

#### Pseudorandom Functions (PRF), PRF constructions, Pseudorandom Permutations (PRP), Feistel networks

##### Chosen-plaintext attack (CPA) 

- Chosen plaintext attacks (CPA-security)
  - What is: the attacker knows a message and its ciphertext
  - Example

    ![](./res/img/116.png)

    - NO! Using properties of XOR we could retrieve `G(k)`, so also decrypt the message `m`
  - Definition

    ![](./res/img/117.png)

    - Adversary wants to distinguish `m0` and `m1`
    - Like in the image it doesn't work, the adversary can distinguish `m0` and `m1` because they use the same key: no deterministic encryption is secure
  - Solution

    ![](./res/img/118.png)

    - I apply an `Enc` function which isn't deterministic, with the same `m` it outputs a different `c`

##### OWFs imply CPA-secure secret key encryption

- Theorem: OWFs imply CPA-secure secret key encryption
  - Definition: Pseudorandom functions (PRFs)

    ![](./res/img/119.png)

    - For every key (seed) there is a function
    - The attacker can do this game poly-many times
    - `b'` is: i guess we are in real case (or random case)
    - Calligraphic r is the set of all possible functions that map `n` bits to `l` bits
    - Same `x`, same `z`; the function is deterministic, is the way it is chosen the function that depends on the seed
  - Theorem explanation

    ![](./res/img/120.png)

    - `r` is random and is transmitted (in clear) with the message (encrypted)
    - For now there is no adversary
    - This is like CBC
    - This works for encrypting strings with fixed length
  - Theorem proof 
    - Real case

      ![](./res/img/121.png)

    - Random case

      ![](./res/img/122.png)

    - Lemma

      ![](./res/img/123.png)

      - Proof

        ![](./res/img/124.png)

        ![](./res/img/125.png)

        ![](./res/img/126.png)

          - Just repeat

        ![](./res/img/127.png)

      - Now we have to show that the two `HYB` are computationally close too
        - What if in the blue world i pick the same `r`?

          ![](./res/img/128.png)

          - I need a green world where there is no message, just the ciphertext is chosen randomly (uniform)
          - Lemma: the blue world is equal to the green world (statistically close)

            ![](./res/img/129.png)

            - Proof

              ![](./res/img/130.png)

              - If this happens then blue world is different from green world (example: i challenge the machine with always the same message, then some ciphertext will be the same!)

              ![](./res/img/131.png)

              - This happens with a probability that is equal to the collision probability; NEGLIGIBLE!
              - In other words:

              ![](./res/img/132.png)

              ![](./res/img/133.png)

                - Triangle inequality: split the modulus of a sum as at most the sum of the modules

              ![](./res/img/134.png)

                - Using bayes

              ![](./res/img/135.png)

##### Constructing PRFs from PRGs: The GGM construction

- Theorem: PRGs implies PRFs (if `G` is a PRG then `F` is a PRF)
  - Explanation

    ![](./res/img/136.png)

      - `G` is a PRG

    ![](./res/img/137.png)

    ![](./res/img/138.png)

      - If there are more bits i add more layers 

    ![](./res/img/139.png)

  - To recap

    ![](./res/img/140.png)

  - Proof: based on lemma

    ![](./res/img/141.png)

    - Proof of lemma using reduction and hybrid

      ![](./res/img/435.jpg)

      ![](./res/img/436.jpg)

  - Proof: of theorem by induction over `n(lambda) = poly(lambda)` (over the input length)

    ![](./res/img/142.png)

    - Lemma

      ![](./res/img/143.png)

      - Proof

        ![](./res/img/144.png)

        - `x` and `y` are respectively the first of `n` bits and the remaining bits

        ![](./res/img/145.png)

        - `i(y)` is the next sample, if already used then it is not the next sample but the one previously used
          - So: `y` is the "seed" of the sample (tuple) and `x` is the `{0,1}` that choses the element of the tuple
        - The attacker can't understand if we used the uniform or the PRG

##### Message authentication codes: Unforgeability against chosen-message attacks (CMA)

- Recall

  ![](./res/img/146.png)

- Definition: What it means for a MAC to be computationally secure? (Unforgeability against chosen-message attacks)

  ![](./res/img/147.png)

  ![](./res/img/148.png)

- Theorem: every PRF yields a fixed-input length MAC

  ![](./res/img/149.png)

  - Proof of lemma 1

    ![](./res/img/150.png)

  - Proof of lemma 2

    ![](./res/img/151.png)

    - Because `A` would have to simply guess the output of a random function `R`

##### Domain extension for PRFs via almost-universal hash functions

- Recap

  ![](./res/img/155.png)

- Domain extension
  - MAC: no fixed input lengths, just input length is a multiple of the key

    ![](./res/img/156.png)

    - Failed attempts

      ![](./res/img/158.png)

      - Xoring is bad because giving as message all 0 will output the key

      ![](./res/img/157.png)

      - First is bad because we could predict the output: `f(m1)` and `f(m2)` concatenated = `f(m1, m2)`
      - Second is bad because: `f(m1, m2, m3)` and f`(m4, m5, m6)`; given that i can forge `f(m1, m5, m3)`
    - Our solution

      ![](./res/img/159.png)

      ![](./res/img/160.png)

      - CRH = Collision resistant hash function

      ![](./res/img/161.png)

        - Definition: Almost universality

          ![](./res/img/162.png)

        - Theorem

          ![](./res/img/163.png)

        - Proof
          - Consider 3 experiments (consider them in the same line)

            ![](./res/img/164.png)

            ![](./res/img/165.png)

          - Proof

            ![](./res/img/166.png)  
            
            ![](./res/img/167.png)    

            ![](./res/img/168.png)

            - They are the same if no collision arises in `HYB`

            ![](./res/img/169.png)

            ![](./res/img/170.png)

            - I don't use the function `h` at each query, i just use it at the end to check if the event happened

            ![](./res/img/171.png)

##### Constructions of almost universal hash functions

###### Information theoretically

- Perfect security but long key

  ![](./res/img/172.png)

  - `F`: filed -> set on which addition, subtraction, multiplication, and division are defined and behave as the corresponding operations on rational and real numbers do
  - `GF`: galois field -> Field that contains a finite number of elements; set on which the operations of multiplication, addition, subtraction and division are defined and satisfy certain basic rules (examples: integers mod p when p is a prime number) 

  ![](./res/img/173.png)

  ![](./res/img/174.png)

- Short key but not perfect

  ![](./res/img/175.png)

  ![](./res/img/176.png)

###### Computationally - CBC-ECBC-XOR MAC

![](./res/img/177.png)

- Domain separation: one `PRF` family, if the input starts with `0` think of it as the first `PRF`, if the input starts with `1` think of it as the second `PRF`
- **CBC MAC**

  ![](./res/img/178.png)

  - Fixed input length
- **ECBC MAC**

  ![](./res/img/179.png)

  - Variable input length
  - Taking the result of CBC and applying `Fk()` we get encrypted CBC
  - Theorem: CBC-MAC directly gives a fixed input length MAC; so if you need just fixed input length you don't need encrypted CBC
- **XOR MAC**

  ![](./res/img/180.png)

  ![](./res/img/181.png)

  ![](./res/img/182.png)

  - `AXU`: almost xor universality
  - Variable input length

##### Pseudorandom permutations

- Domain extension for secure key encryption

  ![](./res/img/183.png)

  - Inverting `F` is not possible in `PRFs`; the solution is using `PRPs`
- **Pseudorandom permutations** (`PRPs`)

  ![](./res/img/184.png)

  ![](./res/img/185.png)

- Strong `PRP`: AES

  ![](./res/img/186.png)

###### PRP design (block cipher): Feistel networks

- Properties
  - Confusion: if you have to reverse engineer it is very hard
  - Diffusion: if i change one pbt of the input then all bits of the output change
- Examples: DES, CAMELIA, BLOWFISH, AES
- Luby-Rackoff construction: take a function and get a permutation such that it is invertible (without inverting the function); if the function is not invertible, using this construction you can invert it
  - Round

    ![](./res/img/187.png)

    ![](./res/img/188.png)

    - `F` is a PRF, is this a PRP?
      - No because the second part of the input is the first part of the output
      - So we are making it invertible but we are destroying pseudorandomness
  - Idea: we could do more rounds! (2)

    ![](./res/img/189.png)

    ![](./res/img/190.png)

    - Is this a PRP?

      ![](./res/img/191.png)

  - Idea: more rounds!

    ![](./res/img/192.png)

    - `r`: number of rounds
  - Luby-Rackoff theorem

    ![](./res/img/193.png)

    - Lemma

      ![](./res/img/194.png)

      ![](./res/img/195.png)
      
      - Given two random functions `F` and `F'` two rounds of Feistel are statistically indistinguishable from a random function `R` from `2n` to `2n` (this is only true if all the queries are `y`-unique, distinct `y`) 
      - Proof

        ![](./res/img/196.png)

        - `T` is `R` (image is wrong)
        - By hybrid argument it suffices to show:

          ![](./res/img/197.png)

        ![](./res/img/198.png)

        ![](./res/img/199.png)

        ![](./res/img/200.png)

        ![](./res/img/201.png)

        - Without the `sum`, image wrong

        ![](./res/img/202.png)

    - Proof

      ![](./res/img/203.png)

      ![](./res/img/204.png)

      - Lemmas

        ![](./res/img/205.png)

        - Proof

          ![](./res/img/206.png)

          ![](./res/img/207.png)

          ![](./res/img/208.png)

        ![](./res/img/209.png)

#### Summetric encryption

##### Modes of operation

- Given a block cipher with fixed input length, how to encrypt long messages? Rely on PRF and PRP

###### ECB mode

![](./res/img/210.png)

![](./res/img/211.png)

- This is bad because it is deterministic; not CPA secure
- To a given portion of message corresponds a portion of the cipher which is static

###### CFB mode

![](./res/img/212.png)

###### CBC mode

![](./res/img/213.png)

- As we can see it has similarities with CBC MAC

###### CTR mode

- Definition

  ![](./res/img/215.png)

  - Think of `r` as an integer
- Theorem (this also proves other modes)

  ![](./res/img/216.png)

  - Proof

    ![](./res/img/217.png)

    - For every message there is a number of blocks
    - For a random key `k`, for every encryption a random `r` is extracted (using `k`); this is added to the number of the block and xored with the block
    - The adversary must understand to which message `*` corresponds the ciphertext `*`

    ![](./res/img/218.png)

    - Replace all `F` with `R`
    - Lemma
    
      ![](./res/img/219.png)
    
    - Lemma

      ![](./res/img/220.png)

      - Proof

        ![](./res/img/221.png)

        ![](./res/img/222.png)

        ![](./res/img/223.png)

        ![](./res/img/224.png)

#### Message authentication

##### Authenticated encryption

- We want
  - Hide the message
  - Authenticate the message and its source

###### Chosen ciphertext attack security (CCA)

- Definition

  ![](./res/img/225.png)

  ![](./res/img/226.png)

  ![](./res/img/227.png)

  - CCA in the sense that if a user sends a ciphertext to a server that decrypts it, the attacker is in the middle and changes the ciphertext and queries the server to know wether the modified ciphertext is a valid one (it can't see the plaintext)
  - CCA == non-malleability: modifying the ciphertext DOESN'T modifies the plaintext
- We obtain CCA security from
  - CPA
  - Authenticity: hard to produce valid ciphertext without knowing the key
    - Valid

      ![](./res/img/229.png)

    - Definition

      ![](./res/img/230.png)

- Theorem: CPA + AUTH = CCA
  - Proof

    ![](./res/img/231.png)

    ![](./res/img/232.png)

  - Constructing a scheme which satisfies CPA and AUTH
    - First try: Encrypt-and-MAC

      ![](./res/img/233.png)

      - **They are secure but they aren't secure together**
        - Proof: try to proof that this scheme is not even CPA secure; `Enc` is CPA secure but `Tag`?

          ![](./res/img/234.png)

          - `Tag` is not supposed to hide the message (infact in the `Tag` game Alice sends to Bob also the plaintext message in clear!)
    - Second try: Tag-then-encrypt

      ![](./res/img/235.png)

      - Not always secure: yes, but usually secure
    - Third try: Encrypt-then-MAC (always works)

      ![](./res/img/236.png)

      ![](./res/img/237.png)

      - `Dec` is `Dec*`, error in image (above)
      - Theorem

        ![](./res/img/238.png)

        ![](./res/img/239.png)

        - Proof

          ![](./res/img/240.png)

          - First: reduction to CPA of `pi1`

            ![](./res/img/241.png)

            ![](./res/img/242.png)

            - And then `b'`
          - Second: reduction to UF-CMA

            ![](./res/img/243.png)

            ![](./res/img/244.png)

            - Is the pair of `c` and `tau` that must be "fresh", but not the single `c` and `tau`!
            - For the same `c` there could be 2 `tau`; this doesn't break UF-CMA

            ![](./res/img/245.png)

            - Assuming STRONG UF-CMA the simulation is perfect
            - See Lesson 12 -> Exercise 1

#### Hash functions

##### Collision resistance

- Collisions in general

  ![](./res/img/249.png)

- In collision resistant hash function families the seed `s` is public (it is like the "description" of the hash function) 
- Collision resistance definition

  ![](./res/img/250.png)

  - Example: MD5, SHA1, SHA2, SHA3...
    - Subtlety: these functions have no seed!
    - So they can't be 100% secure! Whatever the function is there exists an adversary (among all possible adversaries) that knows the collision
    - But this doesn't mean that the function is insecure, we don't know how to find these collisions!

###### Merkle-Damgaard

- Construction paradigm

  ![](./res/img/251.png)

- How it works

  ![](./res/img/252.png)

- Theorem (only works for FIL)

  ![](./res/img/253.png)

  - Proof
    - Let `A` be an adversary that outputs `x = (b_1, ..., b_l') != (b_1', ..., b_l') = x'` such that `H'_s(x) = H'_s(x')`
    - Going backwards there is a largest index `j` such that `(b_j, y_j-1) != (b'_j, y'_j-1)` => `H_s(b_j, y_j-1) = H_s(b'_j, y'_j-1)`
    - This observation requires a simple reduction (exercise)
- Proof and patch for VIL impossibility

  ![](./res/img/254.png)

  - Append to `x` an encoding of its length (`l'`) as a block

    ![](./res/img/255.png)

    - In this case we are working with blocks and not with single bits
- Theorem: the above version is collision resistant for VIL
  - Proof: this can be seen as the hash of a hash of a hash...

    ![](./res/img/256.png)

    ![](./res/img/257.png)

    ![](./res/img/258.png)

    - In other words a collision exists but it's very hard to find

###### Merkle tree

- Take block in groups of 2 and compress them until the root of the tree is reached

  ![](./res/img/259.png)

  - Build compression function and radom oracle model

    ![](./res/img/260.png)

    ![](./res/img/261.png)

    - For AES the proof requires that AES is a random permutation for all keys (ideal cipher model); so that we have a gigantic "truth" table for all pseudorandom seeds (impossible in practice)

### Number theory

#### Modular arithmetic

- Consider `Z_n = {0, 1, 2, ..., n - 1}`: integers `mod n`
- `(Z_n, +)` is a group
  - Identity: exists `0` that appertains to `Z_n` such that `a + 0 = a mod n`
  - Add: `a + b = b + a` appertains to `Z_n`
  - Inverse: for each `a`, there exists a `-a` that appertains to `Z_n` such that `a + (-a) = 0`
- `(Z_n, multiplication)` is NOT a group (the inverse doesn't always exist)
  - Lemma: if `gcd(a, n) != 1` then `a` not invertible with respect to multiplication `mod n`
    - `gcd`: greatest common divisor
  - Proof
    - Assume not `a` is invertible
    - => There exists `b` that appertains to `Z_n` such that `ab` is equivalent to `1 mod n`
    - => `ab = 1 + qn` for `q > 0`
    - => `ab - qn = 1` we get `gcd(a, n)` divides `ab - qn`
    - => `gcd(a, n) = 1` (contraddiction)
  - This motivates Euler totient function
    - Description

      ![](./res/img/262.png)

      - Elements of `Z_n` that are invertible `mod n`
      - `phi(n)`: number of elements that are invertible `mod n`
      - If invertible then `gcd` is `1`
    - Example

      ![](./res/img/263.png)

#### Extended euclidean algorithm

- We will work with `Z*_n`, `Z*_p` and more with huge `n` or `p` (eg: `|p| = 600 bits`, `|n| = 2048 bits`) so we need efficient implementation of operations

  ![](./res/img/264.png)

  - Lemma

    ![](./res/img/265.png)

    - Proof
      - We have `a = qb + (a mod b)`
        - `(a mod b) = r`
        - `q = a/b`
      - => A common divisor of `a` and `b` is also a common divisor of `a - qb = a mod b`
      - A common divisor of `b` and `a mod b` is also a common divisor of `a = qb + a mod b`
  - Theorem: given `a, b` we can compute `gcd(a, b)` in polynomial time; also we can find `u, v` such that `gcd(a, b) = ua + bv` (`gcd(a, n)` equivalent to `ua mod n`) 
    - Proof

      ![](./res/img/266.png)

      ![](./res/img/267.png)

      ![](./res/img/268.png)

      ![](./res/img/269.png)

    - Example

      ![](./res/img/270.png)

      ![](./res/img/271.png)

#### Modular exponentiation

![](./res/img/272.png)

![](./res/img/273.png)

![](./res/img/274.png)

#### Prime numbers

- Theorem (prime number theorem)
  - There are infinite many primes
  - How many primes between `1` and `x`

    ![](./res/img/275.png)

    - There is a good chance of choosing a prime number randomly; but how do we know it is prime? -> Primality testing
      - Theorem

        ![](./res/img/276.png)

- Generating primes

  ![](./res/img/277.png)

- Primes and OWF

  ![](./res/img/278.png)

#### Lagrange theorem

- Theorem

  ![](./res/img/279.png)

  - Proof

    ![](./res/img/280.png)

    ![](./res/img/281.png)

    ![](./res/img/282.png)

    ![](./res/img/283.png)

    ![](./res/img/284.png)

- Corollary

  ![](./res/img/285.png)

  - Proof

    ![](./res/img/286.png)

    ![](./res/img/287.png)

#### Cyclic group

![](./res/img/288.png)

![](./res/img/289.png)

![](./res/img/290.png)

![](./res/img/291.png)

![](./res/img/292.png)

![](./res/img/293.png)

![](./res/img/294.png)

![](./res/img/295.png)

#### Discrete Logarithm (DL) problem

![](./res/img/296.png)

![](./res/img/297.png)

- DL defines a OWF

#### Diffie-Hellman key exchange

![](./res/img/298.png)

- What security?
  - Hard to compute the key: **Computational diffie-hellman assumption**

    ![](./res/img/299.png)

    ![](./res/img/300.png)

    - CDH => DL
    - DL => CDH??
  - The key looks random: **Decisional diffie-hellman assumption**
    - This motivates the decisional DH assumption

    ![](./res/img/301.png)

    ![](./res/img/302.png)

    ![](./res/img/303.png)

    ![](./res/img/304.png)

    ![](./res/img/305.png)

    ![](./res/img/306.png)

    ![](./res/img/307.png)

    ![](./res/img/308.png)

### From number theory to PRGs, PRFs, CRHs, PKE, Signatures

![](./res/img/309.png)

#### PRG from DDH: based on factoring

![](./res/img/310.png)

- Construction from DDH

  ![](./res/img/311.png)

  ![](./res/img/312.png)

  - Theorem: the above PRG is secure under DDH
  - Proof

    ![](./res/img/313.png)

    ![](./res/img/314.png)

    ![](./res/img/315.png)

    ![](./res/img/316.png)

    ![](./res/img/317.png)

#### PRF from DDH

![](./res/img/318.png)

- Theorem

  ![](./res/img/319.png)

#### CRH from DL

![](./res/img/320.png)

![](./res/img/321.png)

![](./res/img/322.png)

### Public-key crypto

#### Public-key encryption

##### How it works

![](./res/img/323.png)

- For now we give it as granted

##### Trapdoor permutations

![](./res/img/324.png)

![](./res/img/325.png)

![](./res/img/326.png)

- Not secure as a PKE
- What is a secure PKE?
  - Assumptions
    - CPA
    - CCA
  - Explanation

    ![](./res/img/327.png)

    - In PKE we know how to encrypt so we can calculate locally the result of `b`; that's why it is no CPA secure and so it's not secure
    - In other words is not safe because it is deterministic (same message, same ciphertext) 
- Theorem: TDPs => CPA PKE

  ![](./res/img/328.png)

  ![](./res/img/329.png)

  ![](./res/img/330.png)

##### RSA

- Definition

  ![](./res/img/332.png)

  ![](./res/img/333.png)

  - RSA => TDP

- Correctness

  ![](./res/img/334.png)

- Security

  ![](./res/img/335.png)

- Gamification

  ![](./res/img/336.png)  

##### Can we get a more efficient PKE? Yes

![](./res/img/337.png)

![](./res/img/338.png)

![](./res/img/339.png)

![](./res/img/340.png)

- This is called OAEP: optimal asymmetric encryption padding
- ROM = random oracle model

##### Can we get PKE from factoring? Rabin's TDP

- CPA PKE from factoring, not practical
  - Reduces to finding TDP from factoring

  ![](./res/img/341.png)

  ![](./res/img/342.png)

  - The last `a` should be a `b`

  ![](./res/img/343.png)

  ![](./res/img/344.png)

  ![](./res/img/345.png)

  - Doing the square (`mod p`) gives us a set with:

    ![](./res/img/346.png)

    - Because we are in fact using `mod p`

  ![](./res/img/347.png)

  ![](./res/img/348.png)

  ![](./res/img/349.png)

  ![](./res/img/350.png)

  ![](./res/img/351.png)

  ![](./res/img/352.png)

  - Lemma

    ![](./res/img/353.png)

    - Proof

      ![](./res/img/354.png)

      ![](./res/img/355.png)

    ![](./res/img/356.png)

- Conclusion: not practical but now we know how to get CCA PKE from factoring without random oracles

##### CPA/CCA PKE from DL (DDH) - Elgamal

- Definition

  ![](./res/img/357.png)

  - All operations are in `G`
- Encryption

  ![](./res/img/358.png)

- Decryption

  ![](./res/img/359.png)

- Correctness

  ![](./res/img/360.png)

- Properties
  - Homomorphic

    ![](./res/img/361.png)

###### Elgamal PKE is CPA secure

- Theorem: Elgamal PKE is CPA secure under DDH
  - Proof

    ![](./res/img/364.png)

    ![](./res/img/365.png)

    ![](./res/img/366.png)

    - Lemma

      ![](./res/img/367.png)

      ![](./res/img/444.jpg)

      - Proof

        ![](./res/img/369.png)

        ![](./res/img/368.png)

    - Lemma

      ![](./res/img/370.png)

###### Elgamal PKE is NOT CCA secure

- Definition

  ![](./res/img/371.png)

  ![](./res/img/372.png)

- Elgamal is not CCA secure

  ![](./res/img/373.png)
  
  ![](./res/img/374.png)

##### CCA PKE from DL (DDH) - Cramer-Shoup

###### Hash Proof Systems - Some sort of zero knowledge proof

- Definition

  ![](./res/img/375.png)

  - `y` is a puzzle, `x` is the puzzle solution; Alice wants to convince Bob that she knows the puzzle solution and that this solution exists
- Example of puzzles

  ![](./res/img/376.png)

- Deeper definition

  ![](./res/img/377.png)

- Correctness

  ![](./res/img/378.png)

- Soundness

  ![](./res/img/379.png)

- `t`-universality
  - Definition

    ![](./res/img/380.png)

    - In other words the distribution of `pi-tilde` is uniform, this is true for each choice of up to `t` statements (`t` is a parameter)
    - So you can't guess `pi-tilde` with a probability that is more than `negl`

###### HPS extension: LABELS

![](./res/img/381.png)

- `L` is `{0,1} star`
- Universality: as before but `y_i` not more distinct so long as the labels are different
- Membership indistinguishable language

  ![](./res/img/382.png)

  ![](./res/img/383.png)

  - Example

    ![](./res/img/384.png)

    ![](./res/img/385.png)

###### Cramer-Shoup construction 

![](./res/img/386.png)

![](./res/img/387.png)

![](./res/img/388.png)

![](./res/img/389.png)

![](./res/img/390.png)

###### Fundamental theorem

![](./res/img/391.png)

- Proof: sequence of games that gradually change
  - Game 0
  
    ![](./res/img/392.png)

  - Game 1

    ![](./res/img/393.png)

  - Lemma

    ![](./res/img/394.png)
  
  - Game 2

    ![](./res/img/395.png)

    ![](./res/img/402.png)

    ![](./res/img/403.png)

    - It's game 2, not 3
  - Lemma

    ![](./res/img/396.png)

  - Game 3

    ![](./res/img/397.png)

    ![](./res/img/404.png)

    ![](./res/img/405.png)

    - `Dec` is the same as game 2
  - Lemma

    ![](./res/img/401.png)

    - Proof

      ![](./res/img/406.png)
      
      ![](./res/img/407.png)

      ![](./res/img/408.png)

  - Game 4

    ![](./res/img/398.png)

    - Dec queries as before
  - Lemma

    ![](./res/img/400.png)

    - Proof

      ![](./res/img/409.png)

      ![](./res/img/410.png)

###### Instantiation

![](./res/img/411.png)

![](./res/img/412.png)

- In setup `omega` is `h1`
- Prove is incorrect; correct one:

  ![](./res/img/414.png)

  - Theorem: `phi 1` as above is 1-universal
    - Proof
      1. Show correctness

         ![](./res/img/413.png)

      2. 1-universality

         ![](./res/img/415.png)

         ![](./res/img/416.png)

         ![](./res/img/417.png)

         ![](./res/img/418.png)

![](./res/img/419.png)

![](./res/img/420.png)

- Theorem: `phi 2` as above is 2-universal
  - Proof
    1. Correctness

       ![](./res/img/421.png)

    2. 2-universality

       ![](./res/img/422.png)

       ![](./res/img/423.png)

       ![](./res/img/424.png)

       ![](./res/img/425.png)

       ![](./res/img/426.png)

#### Digital signatures and Identification schemes

- Message authentication (like MAC) using public key cryptography

  ![](./res/img/430.png)

  - In this case everyone that has the public key can verify the signature
- Definition

  ![](./res/img/431.png)

  ![](./res/img/432.png)

- Constructions

  ![](./res/img/433.png)

##### Full-domain hash (FDH, from TDPs)

- Definition

  ![](./res/img/434.png)

  ![](./res/img/435.png)

    - `H` is modeled as a random oracle
- Theorem: FDH is UF-CMA in the ROM (random oracle model)
  - Proof

    ![](./res/img/437.png)

    ![](./res/img/438.png)

    ![](./res/img/439.png)

    ![](./res/img/440.png)

      - `j` is the index of the random oracle query that represents `m*` (forged message)
  - Analysis of the proof

    ![](./res/img/441.png)

##### ID schemes

- This protocol is used to make Alice convince Bob that she knows `sk` (convince Bob that he is really talking to Alice)

  ![](./res/img/442.png)

- Security (for us): PASSIVE -> Hard to impersonate Alice without knowing `sk`; the adversary just see the messages exchange, can't mess with them
- Definition

  ![](./res/img/443.png)

  ![](./res/img/444.png)

  ![](./res/img/445.png)

  ![](./res/img/446.png)

###### Fiat-Shamir signatures

- Definition

  ![](./res/img/447.png)

- Theorem: the above signature scheme is UF-CMA in the ROM assuming the ID scheme is canonical and is PASSIVE secure
  - Proof

    ![](./res/img/448.png)

    ![](./res/img/449.png)

    ![](./res/img/450.png)

    ![](./res/img/451.png)

  - Analysis of the proof

    ![](./res/img/452.png)

###### ID schemes construction

- Two criteria (sufficient)
  - Honest verifier zero knowledge (HVZK)

    ![](./res/img/453.png)

    - The transcript can be simulated by a turing machine that only knows the public key so honestly computed transcripts reveal nothing on `sk`
  - Special soundness (SS)

    ![](./res/img/454.png)

- Theorem: let `Pi` be a canonical id scheme with HVZK + SS; then `Pi` is passively secure
  - Proof: reduction to SS
    1. Consider a modified experiment 

       ![](./res/img/455.png)

    2. Edit this game (and create one completely identical)

       ![](./res/img/456.png)

    3. Lemma

       ![](./res/img/457.png)

       ![](./res/img/458.png)

       - (On the right of the previous image)

       ![](./res/img/459.png)

       ![](./res/img/460.png)

       - (On the left there is `D`)
       - The proof is not finished
    4. Lemma

       ![](./res/img/461.png)

       ![](./res/img/462.png)

       - (Green is the second execution)
       - We do two runs that aren't independent

       ![](./res/img/463.png)

       ![](./res/img/464.png)

       ![](./res/img/465.png)

       ![](./res/img/466.png)

###### Schnorr ID scheme

![](./res/img/467.png)

- Theorem: the above ID scheme is CANONICAL, COMPLETE (the verifier is always gonna accept the proof), SS (under DL), HVZK

  ![](./res/img/468.png)

  - `beta != beta'`

  ![](./res/img/469.png)

  ![](./res/img/470.png)

  ![](./res/img/471.png)

  - Analysis

    ![](./res/img/472.png)

##### Signatures without random oracle

- Theorem: OWF => UF-CMA digital signatures (signatures are in Minicrypt)

###### Water's signature scheme (based on CDH over bilinear groups)

- Bilinear groups

  ![](./res/img/473.png)

  - `G` is an elliptic curve group
- Water's signature scheme: we are going to use CDH because the DDH doesn't hold here (see exercise)
  - Scheme

    ![](./res/img/475.png)

    ![](./res/img/476.png)

    ![](./res/img/477.png)

  - Theorem: Water's signature scheme is UF-CMA under DDH
    - Proof
      - Completeness

        ![](./res/img/478.png)

      - UF-CMA (reduction to CDH)
        - General

          ![](./res/img/479.png)

        - First step

          ![](./res/img/480.png)

          ![](./res/img/481.png)

        - Second step

          ![](./res/img/482.png)

          ![](./res/img/483.png)

          ![](./res/img/484.png)

          ![](./res/img/485.png)

        - Third step

          ![](./res/img/486.png)

          ![](./res/img/487.png)

          ![](./res/img/488.png)

        - Putting it together

          ![](./res/img/489.png)

          ![](./res/img/490.png)

          ![](./res/img/491.png)

          ![](./res/img/492.png)

          ![](./res/img/493.png)
          
          ![](./res/img/494.png)

          ![](./res/img/495.png)

          ![](./res/img/496.png)

          ![](./res/img/497.png)

          ![](./res/img/498.png)

          ![](./res/img/499.png)

#### Identity based encryption

##### Public key infrastructure (PKI)

![](./res/img/500.png)

![](./res/img/501.png)

##### Identity based encryption

![](./res/img/502.png)

![](./res/img/503.png)

###### How it works

![](./res/img/504.png)

![](./res/img/505.png)

###### Correctness

![](./res/img/506.png)

- Remarks
  - Need a secure channel to exchange `d_ID` to Bob
  - Key escrow: the authority knows the secret key of Bob!!

###### Security: "kind of" CPA security (in public key encryption CPA doesn't imply CCA, still studied now)

- `IND-ID-CPA`

  ![](./res/img/507.png)

  ![](./res/img/508.png)
    
  - `ID*` is the target security we want to attack (for example Bob's one)
  - `ID`: ids generated for the attacker
  - An ID is `IND-ID-CPA` if these games are indistinguishable (can not distinguish wether we encrypted `m_0` or `m_1`)
- Weaker selective security: `A` choses `ID*` before seeing `mpk` (and can never query `ID*`) - WE WILL USE THIS

  ![](./res/img/509.png)

###### Construction: no ROs, using pairings

![](./res/img/510.png)

- Decisional bilinear Diffie-Hellman assumption

  ![](./res/img/511.png)

- Actual construction

  ![](./res/img/512.png)

  ![](./res/img/513.png)

  ![](./res/img/514.png)

  ![](./res/img/515.png)

- Theorem: the above is selective `IND-ID-CPA` under the `DBDH` (decisional bilinear Diffie-Hellmann assumption)
- Proof: reduction tu `DBDH`

  ![](./res/img/516.png)

  ![](./res/img/517.png)

  1. `mpk`

      ![](./res/img/518.png)

  2. Key extraction

      ![](./res/img/519.png)

      ![](./res/img/520.png)

      ![](./res/img/521.png)

      ![](./res/img/522.png)

      ![](./res/img/523.png)

  3. Challenge CTX: `c = (u, v, w)`

      ![](./res/img/524.png)

  - Remark

    ![](./res/img/525.png)

###### Applications

1. Selective `IND-ID-CPA` IBE implies CCA PKE

   ![](./res/img/526.png)

   - (IBE for `ID` in `{0,1}^n`)
   - (`Pi''` is our PKE)

   ![](./res/img/527.png)

   ![](./res/img/528.png)

   ![](./res/img/529.png)

   - Theorem

     ![](./res/img/530.png)

     - Intuition

       ![](./res/img/531.png)

     - Proof

       ![](./res/img/532.png)

       ![](./res/img/533.png)

       - Lemma

         ![](./res/img/534.png)

         - Proof: reduction to UF-CMA of `Pi'`

           ![](./res/img/535.png)

           ![](./res/img/536.png)

           ![](./res/img/537.png)

       - Lemma

         ![](./res/img/538.png)

         - Proof: reduction to selective `IND-ID-CPA`

           ![](./res/img/539.png)

           ![](./res/img/540.png)

           ![](./res/img/541.png)

2. Selective `IND-ID-CPA` implies selective UF-CMA signatures

   ![](./res/img/542.png)

   ![](./res/img/543.png)

   - Theorem

     ![](./res/img/544.png)

     - Proof: reduction to new propert of IBE

       ![](./res/img/545.png)

       ![](./res/img/546.png)

## Exercises

### GENERAL APPROACH TO EXERCISES

- Pinpoint differences
- Prove differences using creativity (not bounded to reality)
- Proofs can also be "stupid" things!

### Exercises done during lessons

- Lesson 1
  - Text
    - Define two time secure encryption
    - Prove one-time pad is not two time secure
  - Solution (temptative)
    - Define two time secure encryption
      - Every algorithm that doesn't reveal anything about the plaintext if the key is used two times is two time secure
    - Prove one-time pad is not two time secure

      ![](./res/img/363.jpg)

      - So using the same `k` two times leaks something of the message
- Lesson 2
  - Text

    ![](./res/img/31.png)

  - Solution
- Lesson 3
  - Ex 1
    - Text

      ![](./res/img/32.png)

      ![](./res/img/33.png)

    - Solution (temptative)

      ![](./res/img/362.jpg)

      - We just took the proof for `t = 2` and extended it to `t = 3`
      - To prove the second step i think that you could use a generic matrix (of size `t`) and show that also for `t-1` the underlined part of the final probability is constant
  - Ex 2
    - Text

      ![](./res/img/48.png)

      ![](./res/img/49.png)

    - Solution
      - "Ex1"

        ![](./res/img/428.jpg)

      - "Ex2": given that `Min entropy (X) = k`

        ![](./res/img/429.jpg)

  - Ex 3 - 4
    - Text (refers to leftover hash lemma theorem proof)

      ![](./res/img/68.png)

    - Solution: by pairwise independence when the input (`x != x'`) is distinct the joint distribution is uniform; so `Pr[H(s,x) = H(s,x')]` is uniform (`2^(-l)`)
- Lesson 4
  - Exercise 1
    - Text

      ![](./res/img/63.png)

    - Solution: i think that you just need to show with a practical example
  - Exercise 2
    - Text

      ![](./res/img/66.png)

    - Solution: if NP doesn't exist (P is NP), then every OWF is invertible, so talking about OWFs doesn't make sense
- Lesson 5
  - Exercise 1: refers to the first, second and third methods
    - Text

      ![](./res/img/75.png)

    - Solution: 3a implies 3b because they're just the same, but written differently; 3c just merges the two games above, the first case is the one in which the adversary plays the normal game (P <= negl), the second is when the adversary choses b flipping a coin, as the challenger does (P = 1/2) => P(A wins) <= negl + 1/2 
  - Exercise 2
    - Text

      ![](./res/img/76.png)
    
    - Solution: given that A isn't a PPT (runs also in non-poly time) then no PRG is safe
  - Exercise 3: refers to game defined in "What is secure encryption"
    - Text

      ![](./res/img/81.png)

    - Solution

      ![](./res/img/80.png)

      - Just extend the game to use 3 messages
  - Exercise 4
    - Text

      ![](./res/img/83.png)

    - Solution

      ![](./res/img/430.jpg)

  - Exercise 5
    - Text

      ![](./res/img/89.png)

    - Solution (lemmas refer to "Proof lemma 1" and "Proof lemma 1")

      ![](./res/img/97.png)

      ![](./res/img/98.png)

      ![](./res/img/99.png)

- Lesson 6
  - Exercise 1
    - Text

      ![](./res/img/112.png)

    - Solution

      ![](./res/img/431.jpg)

  - Exercise 2
    - Text: assume f is a OWF, then g which is defined as (f(x),r) is also a OWF (we need a reduction); r $<-{0,1}^n
    - Solution

      ![](./res/img/432.jpg)

- Lesson 7
  - Exercise 1
    - Text: the object is called combiner

      ![](./res/img/115.png)

    - Solution 

      ![](./res/img/434.jpg)

      - If G2 is "dummy" so is not a PRG that's not a problem, because at least one is a PRG (G1)
      - Even if so we know z2, and so we can achieve to find z1 that's not a problem because z1 is still the result of a secure PRG 
- Lesson 8
  - Exercise 1
    - Text: refers to (Proof: of theorem by induction over `n(lambda) = poly(lambda)` (over the input length))

      ![](./res/img/144.png)

    - Solution

      ![](./res/img/437.jpg)

  - Exercise 2
    - Text: refers to (What it means for a MAC to be computationally secure? (Unforgeability against chosen-message attacks))

      ![](./res/img/150.png)

    - Solution

      ![](./res/img/438.jpg)

      ![](./res/img/439.jpg)

  - Exercise 3
    - Text

      ![](./res/img/152.png)

    - Solution
  - Exercise 4
    - Text

      ![](./res/img/153.png)

      ![](./res/img/154.png)

    - Solution
- Lesson 9
  - Exercise 1
    - Text: refering to (Domain extension -> MAC: no fixed input lengths, just input length is a multiple of the key)

      ![](./res/img/166.png)

    - Solution: simple reduction to the real case, you have that you can't distinguish from REAL to RAND
  - Exercise 2
    - Text: prove that CBC MAC is not variable input length
    - Solution

      ![](./res/img/443.jpg)

- Lesson 10
  - Exercise 1
    - Text: prove that `3` is not a STRONG PRP

      ![](./res/img/193.png)

    - Solution
  - Exercise 2
    - Text: from (lemmas of Luby-Rackoff theorem proof)

      ![](./res/img/205.png)

    - Solution
  - Exercise 3
    - Text: from (lemmas of Luby-Rackoff theorem proof)

      ![](./res/img/209.png)

    - Solution
- Lesson 11
  - Exercise 1
    - Text

      ![](./res/img/214.png)

    - Solution
  - Exercise 2
    - Text: from (CTR mode -> theorem proof)

      ![](./res/img/219.png)

    - Solution
  - Exercise 3
    - Text

      ![](./res/img/228.png)

    - Solution
- Lesson 12
  - Exercise 1
    - Text

      ![](./res/img/246.png)

      - Guideline (general)
        - If this doesn't hold, it exists a tag that satisfies the weak notion but not the strong one
        - Start from a tag that satisfies the weak one, then focus on finding the difference between the weak and strong notion, then change the tag so that it doesn't satisfy the strong notion (but keep an eye open on not losing the weak property)
    - Solution
      - Construction

        ![](./res/img/247.png)

      - Proof

        ![](./res/img/248.png)

- Lesson 13
  - Exercise 1
    - Text: refers to Merkle-Damgaard
      - Theorem (only works for FIL)

        ![](./res/img/253.png)

        - Proof
          - Let `A` be an adversary that outputs `x = (b_1, ..., b_l') != (b_1', ..., b_l') = x'` such that `H'_s(x) = H'_s(x')`
          - Going backwards there is a largest index `j` such that `(b_j, y_j-1) != (b'_j, y'_j-1)` => `H_s(b_j, y_j-1) = H_s(b'_j, y'_j-1)`
          - This observation requires a simple reduction (exercise)
    - Solution
  - Exercise 2
    - Text: Merkle-Damgaard insecure for VIL
    - Solution

      ![](./res/img/254.png)

- Lesson 15
  - Exercise 1
    - Text

      ![](./res/img/331.png)

      - Missing part: PKE
    - Solution
- Lesson 17
  - Exercise 1
    - Text (from Elgamal PKE is CPA secure under DDH -> Proof -> Lemma)

      ![](./res/img/367.png)

    - Solution

      ![](./res/img/369.png)

      ![](./res/img/368.png)

- Lesson 19
  - Exercise 1
    - Text (from Full-domain hash (FDH, from TDPs) definition)
      - What about removing `H`? -> Not secure
    - Solution

      ![](./res/img/436.png)

      - I could chose some `sigma` and then compute the `m` that corresponds to that `sigma`
- Lesson 21
  - Exercise 1
    - Text and solution (from Water's signature scheme (based on CDH over bilinear groups) -> Bilinear groups)

      ![](./res/img/474.png)

- Lesson 23
  - Exercise 1
    - Text (from Identity based encryption -> Construction -> 2, but i think that maybe is a general exercise)

      ![](./res/img/547.png)

    - Solution: reduction to OW-ID-CPA (an `A'` is missing on the top left)

      ![](./res/img/548.png)

      ![](./res/img/549.png)

  - Exercise 2
    - Text (from Identity based encryption -> Construction -> 2, but i think that maybe is a general exercise)

      ![](./res/img/550.png)

### Exercises done during last lesson (EXAMPLE EXAM)

- Exercise 1
  - Text

    ![](./res/img/551.png)

  - Solution

    ![](./res/img/552.png)

- Exercise 2
  - Text

    ![](./res/img/553.png)

  - Solution

    ![](./res/img/554.png)

    ![](./res/img/555.png)

    ![](./res/img/556.png)
    
    ![](./res/img/557.png)

    ![](./res/img/558.png)

    ![](./res/img/559.png)

    ![](./res/img/560.png)

    ![](./res/img/561.png)

    ![](./res/img/562.png)

- Exercise 3
  - Text

    ![](./res/img/563.png)

  - Solution

    ![](./res/img/564.png)

    - Reduction

      ![](./res/img/565.png)

- Exercise 4
  - Text and solution

    ![](./res/img/566.png)

- Exercise 5
  - Text

    ![](./res/img/567.png)

  - Solution

    ![](./res/img/568.png)

- Exercise 6
  - Text

    ![](./res/img/569.png)

  - Solution

    ![](./res/img/570.png)

- Exercise 7
  - Text

    ![](./res/img/571.png)

  - Solution

    ![](./res/img/572.png)

    ![](./res/img/573.png)

    ![](./res/img/574.png)

- Exercise 8
  - Text: RUF-RMA

    ![](./res/img/575.png)

  - Solution
    - A: yes, reduction

      ![](./res/img/576.png)

    - B

      ![](./res/img/577.png)

    - C: reduction to RSA assumption

      ![](./res/img/578.png)

      ![](./res/img/579.png)

- Exercise 9
  - Text

    ![](./res/img/580.png)

  - Solution

    ![](./res/img/581.png)

### Exercise done by ourself

- Homework 1 - 18/11/2016 - Some doubts

  ![](./res/img/440.jpg)

- Exercise 25 of the database (to show how hybrid argument works)

  ![](./res/img/441.jpg)

  ![](./res/img/442.jpg)