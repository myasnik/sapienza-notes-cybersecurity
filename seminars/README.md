# Seminars

[[_TOC_]]

## Theory

### Disclaimer

- **Innovation and new business models** is the guideline to create the report
- Other arguments change year by year, they're just to give some ideas on which basing your final report (but you could also do something not correlated to those topics)

### Innovation and new business models

- What is innovation
  - Theory of innovation
    - Schumpeterian interpretation
      - Markets are inefficient; bad for capitalism
      - Entrepreneur: agent of economic growth, fix market inefficiency
      - Innovation is creating economic value: "Countries that innovate would grow wealthier, those that did not would stagnate"
      - To make space to new industries you must destruct old industries (innovation is "creative destruction")
        - Cycle
          - Early adopters
          - Transfer of value and efficiency gain
          - Decrease of the cost of production -> decrease cost to consumer
          - Economic instability while the transfer of value occurs
          - Economic growth and imitation (imitation = people that copy the main source of innovation, competitors)
    - Digital disruption
      - Disruptive innovation: smaller company challenges a bigger one (Christensen)
        - To face this bigger companies must invest in disruptive innovations
    - Godin's classification: innovation could also be non-destructive
  - Innovation comes from the internal: motivation and personality that come from the inside brings innovation
- Business models
  - What are them
    - Stories that explain how enterprises work
    - Who is the customer?
    - What are the customers values?
    - How do we make money in this business?
    - What is the economic logic behind this business model?
  - Dynamic
  - Business model canvas: framework to build a business model

    ![](./res/img/1.png)

    - Put yourself in the shoes of your customer
    - People don't know what they need (deeply)
    - Understand your market
      - SWOT analysis
      - PESTLE analysis
      - Porter's five forces analysis
    - Imagine your product
    - Define what is unique about your business model
    - Devise an economic strategy
    - Balancing costs and revenues
    - Measuring success

### L'ecosistema della scena hacker [ITA]

![](./res/img/2.png)

![](./res/img/3.png)

- Il cybercrime influenza la vita reale
- Ragioni insicurezza
  - Il software ed i sistemi sono intrinsecamente insicuri
  - Il software è sviluppato troppo velocemente
  - Le aziende si fidano della brochure e non fanno test
  - Il landscape delle applicazioni è ampissimo
  - Il budget scarso determina acquisizione di tecnologie a basso costo
  - Le competenze di addetti al lavoro sono scarse
  - Stratificazione delle tecnologie (cose vecchie usate come base per le nuove)
  - Gli utenti sono il punto di ingresso principale
- Vulnerabilità più frequenti
  1. Credenziali deboli
  2. Sistemi di autenticazione deboli
  3. Abuse of functionality (funzionalità esistenti ma non documentate)
  4. XSS
  5. SQLi

### Blockchain as a process execution infrastructure

- Blockchain
  - Transactions: move currency from a source to a destination using a signature
  - Block: boxes of transactions; boxes are linked together
  - Ledger: ordered collection of tramsactions (blocks)
    - The order count: avoid double spending
    - Distributed: if not then the network is at risk
      - Problems
        - Latency
        - Consensus
  - Mining
    - Types
      - PoW: publish the next block if you solve a computationally intensive puzzle on the block
      - PoS: decide on the next block to be published and put cryptoassets at stake for it
      - ...
    - In general
      - Hard to compute but easy to check
    - To make this work we need a currency (to make people mine)
- Smart contracts
  - What are: pieces of code
  - Everyone could create a smart contract
  - Ethereum virtual machine (EVM)
    - Resides in every node and in no one: everyone is part of the machine
  - To mine we just have to execute the smart contract, the more we execute the more we are payed from the smart contract owner
  - Token: something that are given to you while running contracts (connected to mining above); these are spendible 
- Private / public / permissioned / permissionless
- The web is moving to distributed applications, also based on blockchains
- Do i need a blockchain?
  - Wurst & Gervais scheme

### Malware reverse engineering: foundations

- Why reversing
  - Cracking software licenses
  - Code lifting (steal code reversing it)
  - Bypass security products
  - Vulnerability research
  - Malware reversing
- Reversing malware vs reversing software
  - Malware
    - Why it does this?
      - A lot of questions
  - Software
    - A bit more straight-forward
- Practice
  - Sections
    - `rodata`: static strings
    - `reloc`: contains information about relocation
    - ...
  - Loading and running binary
    1. Relocation
    2. Load interpreter
    3. Interpreter takes control and 
    4. ...
  - Binary disassembly
    - Methods
      - Linear: iterates through all code segments in a binary decoding all bytes consecutively
        - Problem: misinterpret inlined data s code (desynchronization)
      - Recursive: start from the start and recursively follow all jumps to recover the code
        - Problem: hard to follow all
    - Function identification (usually)
      - Prologue: `push ebp; mov ebp,esp`
      - Epilogue: `leave, ret`
    - Control-flow graph: we know what it is
- Some samples analysis
  - ...

## Project

- Note: check slides of the first lesson (second part) to create the report 